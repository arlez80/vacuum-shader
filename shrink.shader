/*
	吸い込みシェーダー by あるる（きのもと 結衣） @arlez80
	Space Vacuum Shader by Yui Kinomoto

	MIT License
*/
shader_type spatial;
render_mode unshaded;

const float PI = 3.1415926535;

uniform float power = 0.5;
uniform float vacuum_power = 0.05;
uniform vec4 albedo : hint_color = vec4( 0.5, 0.5, 0.5, 1.0 );

void fragment( )
{
	vec4 CENTER_VIEW = INV_CAMERA_MATRIX * vec4( WORLD_MATRIX[3].xyz, 1.0 );
	vec2 diff = ( normalize( -CENTER_VIEW ).xy - VIEW.xy ) * ( -CENTER_VIEW.z );
	float to_center = length( diff );

	ALBEDO = albedo.rgb * textureLod( SCREEN_TEXTURE, SCREEN_UV + ( normalize( diff ) * sin( to_center * 2.0 * PI ) * power ) * vacuum_power, 0.0 ).rgb;
	ALPHA = albedo.a;
}
